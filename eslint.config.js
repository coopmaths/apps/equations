import neostandard from 'neostandard'

export default neostandard({
  rules: {
    quotes: ['error', 'single']
  }
})
