import Fraction from "fraction.js";
import Equation from "./Equation";
import Trinome from "./Trinome";
import { randInt } from "./utils";

export default function createEquations (): Equation[] {
    const equations: Equation[] = []
    // ax = b avec a > 0
    let a = randInt(2, 10)
    let b = randInt(-10, 10, [0, -1, 1])
    const left1 = Trinome.createAx(a)
    const right1 = Trinome.createConstant(b)
    equations.push(new Equation(left1, right1))

    // x + a = b avec a > 0
    a = randInt(2, 10)
    b = randInt(-10, 10, [0, -1, 1])
    const left2 = Trinome.createAxPlusB(1, a)
    const right2 = Trinome.createConstant(b)
    equations.push(new Equation(left2, right2))

    // x + a = b avec a < 0
    a = randInt(-10, -2)
    b = randInt(-10, 10, [0, -1, 1])
    const left3 = Trinome.createAxPlusB(1, a)
    const right3 = Trinome.createConstant(b)
    equations.push(new Equation(left3, right3))

    // -x = b avec
    b = randInt(-10, 10, [0, -1, 1])
    const left4 = Trinome.createAx(-1)
    const right4 = Trinome.createConstant(b)
    equations.push(new Equation(left4, right4))

    // ax = b avec a < 0
    a = randInt(-10, -2)
    b = randInt(-10, 10, [0, -1, 1])
    const left5 = Trinome.createAx(a)
    const right5 = Trinome.createConstant(b)
    equations.push(new Equation(left5, right5))

    // ax + b = c
    a = randInt(-10, 10, [0, -1, 1])
    b = randInt(-10, 10, [0, -1, 1])
    let c = randInt(-10, 10, [0, -1, 1])
    const left6 = Trinome.createAxPlusB(a, b)
    const right6 = Trinome.createConstant(c)
    equations.push(new Equation(left6, right6))

    // ax + b = cx
    a = randInt(-10, 10, [0, -1, 1])
    b = randInt(-10, 10, [0, -1, 1])
    c = randInt(-10, 10, [0, -1, 1, a])
    const left7 = Trinome.createAxPlusB(a, b)
    const right7 = Trinome.createAx(c)
    equations.push(new Equation(left7, right7))

    // ax + b = cx + d
    a = randInt(-10, 10, [0, -1, 1])
    b = randInt(-10, 10, [0, -1, 1])
    c = randInt(-10, 10, [0, -1, 1, a])
    let d = randInt(-10, 10, [0, -1, 1])
    const left8 = Trinome.createAxPlusB(a, b)
    const right8 = Trinome.createAxPlusB(c, d)
    equations.push(new Equation(left8, right8))

    // x/a + b = c
    a = randInt(2, 5)
    b = randInt(-10, 10, [0, -1, 1])
    c = randInt(-10, 10, [0, -1, 1])
    const left9 = new Trinome(0, new Fraction(1, a), b)
    const right9 = Trinome.createConstant(c)
    equations.push(new Equation(left9, right9))

    // x/a + b = cx + d
    a = randInt(2, 5)
    b = randInt(-10, 10, [0, -1, 1])
    c = randInt(-10, 10, [0, -1, 1])
    d = randInt(-10, 10, [0, -1, 1])
    const left10 = new Trinome(0, new Fraction(1, a), b)
    const right10 = Trinome.createAxPlusB(c, d)
    equations.push(new Equation(left10, right10))

    return equations
}